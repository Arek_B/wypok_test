import Vapor

final class Routes: RouteCollection {
    let view: ViewRenderer
    init(_ view: ViewRenderer) {
        self.view = view
    }

    func build(_ builder: RouteBuilder) throws {
        // POSTS
        builder.get { req in
            let viewModel = try PostsViewModel(pageNumber: 0)

            guard viewModel.posts.count > 0 else {
                return Response(redirect: "prepare")
            }

            return try self.view.make("posts", ["viewModel": viewModel])
        }

        builder.get("", Int.parameter) { req in
            let page = try req.parameters.next(Int.self)
            let viewModel = try PostsViewModel(pageNumber: page)

            return try self.view.make("posts", ["viewModel": viewModel])
        }

        builder.post("") { req in
            guard let text = req.data["body"]?.string else {
                    return Response(status: .badRequest)
            }

            if let user = try UserManager.shared.user() {
                let post = Post(text: text, user: user)
                try post.save()
            }

            return Response(redirect: "/")
        }

        builder.get("prepare") { req in
            try FakeContentHelper.generate()
            return Response(redirect: "/")
        }

        builder.get("info") { req in
            return req.description
        }

        // USERS
        builder.get("users") { req in
            let list = try User.all()
            return try self.view.make("users", ["userlist": list.makeNode(in: nil)])
        }

        builder.post("users") { req in
            guard let username = req.data["username"]?.string,
                  let avatarUrl = req.data["avatarUrl"]?.string else {
                return Response(status: .badRequest)
            }

            let user = User(username: username, avatarUrl: avatarUrl)
            try user.save()
            
            return Response(redirect: "/users")
        }
    }
}
