//
//  UserManager.swift
//  App
//
//  Created by Arkadiusz Banaś on 03/04/2018.
//

import Foundation

class UserManager {

    static let shared = UserManager()

    func user() throws -> User? {
        return try User.find(1)
    }
}
