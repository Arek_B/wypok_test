@_exported import Vapor

extension Droplet {
    public func setup() throws {
        try self.setupRoutes()
    }

    private func setupRoutes() throws {
        let routes = Routes(view)
        try collection(routes)

        let apiPostController = ApiPostController()
        apiPostController.addRoutes(to: self)

        let apiUsersController = ApiUsersController()
        apiUsersController.addRoutes(to: self)
    }
}
